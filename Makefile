PROG =	hybrd.X

SRCS =	main.f90 zero_finder_module.f90 hybrd1.f

OBJS =	main.o zero_finder_module.o hybrd1.o

LIBS =	

FC = ifort
FFLAGS = -O
F90 = ifort
F90FLAGS = -O
LDFLAGS = 
all: $(PROG)

$(PROG): $(OBJS)
	$(F90) $(LDFLAGS) -o $@ $(OBJS) $(LIBS)

.PHONY: clean
clean:
	rm -f $(PROG) $(OBJS) *.mod

.SUFFIXES: $(SUFFIXES) .f .f90 .F90 .f95

.f90.o .f95.o .F90.o:
	$(F90) $(F90FLAGS) -c $<

.f.o:
	$(FC) $(FFLAGS) -c $<

main.o: zero_finder_module.o
