module zero_finder
   implicit none
   contains
subroutine find_zero ()
    integer                        :: n, lwa, info
    doubleprecision, allocatable   :: x(:), fvec(:), wa(:)
    doubleprecision                :: tol
    external hybrd1
    n=1!2
    lwa = (n*(3*n+13))/2
    allocate( x(n), fvec(n), wa(lwa) )
    tol = 1.0E-8
!  write(0,*) ' initial estimate of x?'
x(1) = 2.0d0
!x(2) = 1.0d0
  call hybrd1(fcn,n,x,fvec,tol,info,wa,lwa)
  write(0,*) ' hybrd1 finished with info = ', info
  write(0,*) ' fvec = '
  write(0,'(e)') fvec
  write(0,*) ' x = '
  write(0,'(e)') x
end subroutine find_zero

subroutine fcn(n,x,fvec,iflag)
    integer, intent(in)             :: n,iflag
    doubleprecision, intent(in)     :: x(n)
    doubleprecision, intent(out)    :: fvec(n)
    !fvec = 100.0d0 * (x(2) - x(1)**2)**2 + (1.0d0-x(1))**2
     fvec = X**2 + X - 2.0
end subroutine fcn


end module zero_finder
