#This is a simple example of how to use hybrd1 to find zeros.
I got the library from [netlib](http://www.netlib.org/cgi-bin/netlibfiles.pl?filename=/minpack/hybrd.f)
##To get this code and run it just do the following:

```BASH
mkdir bitbucket
cd bitbucket
git clone git@bitbucket.org:ignacio82/hybrd1.git
cd hybrd1
make
./hybrd.X
```